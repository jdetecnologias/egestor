import React, {Component} from 'react';
import {AutoComplete} from 'primereact/autocomplete';
import {connect} from 'react-redux' 
import{bindActionCreators} from 'redux'
//import {importarProdutos} from '../../pages/estoque/cadastrar.produto.action'

class SelectMultiItem extends Component {

    constructor() {
      super()
      this.makeItem = this.makeItem.bind(this)
    }

    componentWillMount() {
		
    }
    makeItem(item,field,label){
        return (<li valor={item[field]}>{item[label]}</li>)
    }

    render() {
       
        return (
            <div>
             
                <ul>
                    {this.props.objeto.map((item,indice)=>{
                       return (<li><input type='checkbox' value={item[this.props.field]}/> {item[this.props.label]}</li>)
                    })}
                </ul>
            </div>


        )
    }
}

const mapStateToProps = state => ({produtos: state.cadastroProduto.produtos})
//const mapDispatchToProps = dispatch => bindActionCreators({importarProdutos},dispatch)
export default connect(mapStateToProps)(SelectMultiItem)


/*<AutoComplete 
value={this.state.produto} 
inputClassName='form form-control' 
suggestions={this.state.filteredProdutos} 
completeMethod={this.filterProdutos} 
size={30} 
minLength={1}
placeholder="Digite a descrição do produto" 
dropdown={false} 
field='descricaoCompleta' 
itemTemplate={this.itemTemplate.bind(this)}
 onChange={(e) => this.setState({produto: e.value})} />
*/