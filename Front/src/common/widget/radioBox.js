import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import If from  '../operator/if'
import Grid from '../layout/grid'
import Row from '../layout/row'

export default class AutoComplete extends React.Component{

	constructor(props){
		super(props)
		this.state = {sArrayList:[],showSuggestion: false,pControlText:'', text:'',clickedInside:false}	
	}

	hideSuggestion(){
		this.setState({...this.state, showSuggestion:false})
	}
render(){
		return(
			<Grid cols={this.props.cols || '12'} className='autoComplete'>
				<label className='col-12 text-center'>{this.props.label}</label>
				<fieldset >
					<input type='text' value={this.controlText()?this.props.text:this.state.text}  onChange={(e)=>this.filterArrayList(e)} className='form-control'/>		
					<span onClick={this.showAllRecords} className="pi pi-fw pi-chevron-down p-c p-button-icon-left" style={{position:'absolute', left:'90%',top:'60%',cursor:'pointer'}}></span>
					<If test={this.state.showSuggestion}>
						<ul onClick={this.setClickedInside} className='col-12' style={{position:'absolute',zIndex:'9999',backgroundColor:'white', border:'1px grey solid',top:'76%'}}>
						{
							this.state.sArrayList.map((it)=>( 
								<li tipo={this.props.tipo} onClick={this.setProduto} value={it[this.props.fieldValue]}>{it[this.props.field]}</li>
								)
							)		
						}
						</ul>
					</If>
				</fieldset>
			</Grid>
		)
	}
}