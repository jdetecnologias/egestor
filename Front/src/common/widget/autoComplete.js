import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import If from  '../operator/if'
import Grid from '../layout/grid'
import Row from '../layout/row'
import {definirVariacao} from '../../pages/produtos/variacao.produtos.action'

class AutoComplete extends React.Component{

	constructor(props){
		super(props)
		this.state = {sArrayList:[],showSuggestion: false,pControlText:'', text:'',clickedInside:false,selectedItens:[]}
		this.filterArrayList = this.filterArrayList.bind(this)
		this.hideSuggestion = this.hideSuggestion.bind(this)
		this.setProduto = this.setProduto.bind(this)
		this.controlText = this.controlText.bind(this)
		this.lControlTxt = ''
		this.showAllRecords = this.showAllRecords.bind(this)
		this.definirVariante = this.definirVariante.bind(this)
		this.retornarMultiTextItensSelecionado = this.retornarMultiTextItensSelecionado.bind(this)
		this.EstaItemSelecionado = this.EstaItemSelecionado.bind(this)

		const $this = this
		document.body.onclick = (e)=>{
			const path = e.path

			let result = false
			Array.prototype.map.call(path,function(element){

				if(element.parentNode != null){
					if(element.classList.contains('autoComplete')){
						result = true
					}
				}
			})

			if(!result){
				$this.hideSuggestion()
			}
		}
	}

	hideSuggestion(){
		this.setState({...this.state, showSuggestion:false})
	}
	filterArrayList(e){
		const minLength = this.props.minLength || 1
		const aArrayList = this.props.pArrayList
		const text = e.target.value
		let variante = this.props.variacao
		const aNewArr = aArrayList.filter(it=>{
			return it[this.props.field].toLowerCase().search(text.toLowerCase())> - 1
		})

		this.setState({...this.state, sArrayList:aNewArr, showSuggestion:text.length >= minLength,text: e.target.text})
	}
	showAllRecords(){
		this.setState({...this.state, sArrayList:this.props.pArrayList, showSuggestion:!this.state.showSuggestion})
	}
	controlText(){		
		if(this.lControlTxt !== this.props.text){
			this.lControlTxt = this.props.text
			return true
			//this.setState({...this.state, pControlText: this.props.text, text:this.props.text})
		}
	}
	definirVariante(variante){
		this.props.definirVariacao(variante)
	}
	retornarMultiTextItensSelecionado(arr){
		let multiTexto = ''
		for(let i = 0 ; i<arr.length;i++){
			multiTexto += arr[i].nome || arr[i].valor
			if((i+1) < arr.length){
				multiTexto += ','
			}
		}
		return multiTexto
	}
	EstaItemSelecionado(_id){
		if(this.props.selectedItens[this.props.tipo]){
			let qtdFind = 0
			this.props.selectedItens[this.props.tipo].map(el=>{
				if(el._id == _id){
					qtdFind++
				}
			})
			if(qtdFind <= 0){
				return false
			}else{
				return true
			}
		}
	}

	setProduto(item){
		this.props.onClickItem(this.props.tipo, item, this.props.multi || false)		
		this.setState({...this.state,
						showSuggestion:false,
						text: this.props.selectedItens[this.props.tipo].length <=  0 ?
								'':
								this.props.selectedItens[this.props.tipo].length ==  1 ?
								this.props.selectedItens[this.props.tipo][0].nome || 
								this.props.selectedItens[this.props.tipo][0].valor:
								this.retornarMultiTextItensSelecionado(this.props.selectedItens[this.props.tipo])
					})
	}

	render(){
		return(
			<Grid cols={this.props.cols || '12'} className='autoComplete'>
				<label className='col-12 text-center'>{this.props.label}</label>
				<fieldset >
					<input type='text' value={this.state.text} onChange={(e)=>this.filterArrayList(e)} className='form-control'/>		
					<span onClick={()=>this.showAllRecords()} className="pi pi-fw pi-chevron-down p-c p-button-icon-left" style={{position:'absolute', left:'90%',top:'60%',cursor:'pointer'}}></span>
					<If test={this.state.showSuggestion}>
						<ul onClick={this.setClickedInside} className='col-12' style={{position:'absolute',zIndex:'9999',backgroundColor:'white', border:'1px grey solid',top:'76%'}}>
						{
							this.state.sArrayList.map((it,indice)=>{ 
								if(this.EstaItemSelecionado(it._id)){
									
									return (<li className='bg-success text-white' tipo={this.props.tipo} onClick={()=>this.setProduto(it)} value={it[this.props.fieldValue]}>{it[this.props.field]}</li>)
								}else{
									return (<li className='' tipo={this.props.tipo} onClick={()=>this.setProduto(it)} value={it[this.props.fieldValue]}>{it[this.props.field]}</li>)
								}
							}
							)		
						}
						</ul>
					</If>
				</fieldset>
			</Grid>
		)
	}
}

const mapStateToProps = state => ({variacao: state.variacao})
const  mapDispatchToProps = dispatch => bindActionCreators({definirVariacao},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(AutoComplete)