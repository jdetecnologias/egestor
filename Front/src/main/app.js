import React from 'react'	
import Header from '../template/header'
import Footer from '../template/footer'
import Row from '../common/layout/row'
import Painel from '../pages/painel'


export default props=>(
	<div  className='container-fluid'>
		<Header/>
		<Row className='row'>
			<Painel/>
		</Row>
		<Footer/>
	</div>
)