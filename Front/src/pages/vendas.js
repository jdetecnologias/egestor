import React from 'react'
import Grid from '../common/layout/grid'
import Page from '../common/layout/page' 
import Row from '../common/layout/row'
import If from '../common/operator/if'
import AddItem from '../component/adicionarItem'
import ExcItem  from '../component/excluirItem'
import {Input, Form,Select} from '../common/layout/form'
import {optionProdutos,resetarCamposSelect,obterTamanhoGrid as OBT,controlarTamanhoTela as CTT} from '../common/operator/funcoes'
import axios from 'axios'

class Venda extends React.Component{
	
	constructor(props){
        super(props)
        this.state = {
            vendas:[],
            grid:[],
            listaExclusao:[], colunas:[],
            LayoutColunas:{
                            cliente:        {label: 'Cliente'},
                            valor:          {label: 'Valor', somar:true}, 
                            data_venda:     {label:'Dt. Venda'}, 
                            data_pagamento: {label: 'Dt. Pgto'}, 
                            status:         {label: ' Status'}
                }
        }
        this.valorTotal = 0;
        this.obterVendas = this.obterVendas.bind(this)
        this.adicionarVenda = this.adicionarVenda.bind(this)
        this.excluirVenda = this.excluirVenda.bind(this)    
        this.incluirExclusao = this.incluirExclusao.bind(this)   
        this.listaColunas = this.listaColunas.bind(this)
        this.EnviarMudanca = this.EnviarMudanca.bind(this)
        this.somarColunasArray = this.somarColunasArray.bind(this)
        this.montarGridCompleta = this.montarGridCompleta.bind(this)
        this.atualizarVendas = this.atualizarVendas.bind(this)
    }
    
    obterVendas(){
       axios.get('http://18.217.144.66:3003/vendas').then((vendas)=>{
            this.atualizarVendas(vendas)
    })
    }

    atualizarVendas(vendas){
        try {
            const dados = JSON.parse(vendas.request.responseText)
            console.log(dados)
            const colunas = Object.keys(dados[0])
            const grid = this.montarGridCompleta(vendas.data, colunas)  
                 
            this.setState({...this.state, vendas: dados, 
                colunas,
                 grid,
                 listaExclusao:[]
})       
        } catch (error) {
            this.setState({...this.state, vendas: [], 
                colunas:[],
                 grid:[],
                 listaExclusao:[]       
        })

    

    }
}

    adicionarVenda(){
        axios.post('http://18.217.144.66:3003/vendas').then(vendas=>this.atualizarVendas(vendas))  
    }

    excluirVenda(){
        if(this.state.listaExclusao.length == 0){
            alert('Nenhum item foi selecionado');
        }else{
            const resp = window.confirm('Você tem certeza que deseja apagar esse registros?')
            if(resp){
                axios.delete('http://18.217.144.66:3003/vendas/'+this.state.listaExclusao.join(',')).then(vendas=>this.atualizarVendas(vendas)) 
            }
        }
    }

    EnviarMudanca(id,col,valor){
        axios.put(`http://18.217.144.66:3003/vendas/${id}/${col}/${valor}`,{id,col,valor},{headers:{'Content-Type': 'application/json'}}).then(vendas=>this.atualizarVendas(vendas))
    }

    listaColunas(){
          const colunas = Object.keys(this.state.vendas[0]);
          console.log('colunas',colunas)
        this.setState({...this.state, colunas })

    }
    somarColunasArray(dados){
        let objSoma = {}
        const LayoutColunas = this.state.LayoutColunas
        dados.map(v=>{        
            Object.keys(LayoutColunas).map(l=>{ 
                    if(LayoutColunas[l].somar){
                        if(typeof(objSoma[l]) !== 'undefined'){
                            objSoma[l] =  objSoma[l] + v[l]
                        }
                        else{
                            objSoma[l] =  v[l] 
                        }
                    }
                    else{
                        objSoma[l] = ''
                    }
            })
        })

        return objSoma

    }

    montarGridCompleta(dados, colunas){
        let Obj = {}      
    
        colunas.map(c=>{
            Obj[c] = c
        })
        const Header = [].concat(Obj)
        const Footer = [].concat(this.somarColunasArray(dados))  
        const grid = Header.concat(dados).concat(Footer)

        return grid;
  
    }

    incluirExclusao(id){
        let listaExclusao = this.state.listaExclusao;
        listaExclusao.push(id);
        this.setState({...this.state, listaExclusao})
    }
    componentDidMount(){
        this.obterVendas()
      //  this.listaColunas()
      /*
                                                                  <td><InputVenda type='text'  value={v.cliente}/></td>
                                                            <td><InputVenda type='text' value={v.valor}/></td>
                                                            <td><InputVenda type='date' value={v.data_venda}/></td>
                                                            <td>{v.status}</td></tr>
      */ 
    
    }
    
	render(){
        console.log(this.state.listaExclusao)
		return(
					<Grid cols='12'>
                        <h3>Vendas</h3>
                        <table className='table table-sm'>

                            
                            {
                                this.state.grid.map((v,k) => {
                                    if(k == 0){
                                      return(  <tr>
                                            <td>Item</td>
                                            {Object.keys(v).map(col=>typeof(this.state.LayoutColunas[col]) !== 'undefined' ? <td>{this.state.LayoutColunas[col].label}</td>: '')}
                                        </tr>
                                      )
                                    }
                                    else if(k === (this.state.grid.length-1)){
                                        return(  <tr>
                                            <td></td>
                                            {Object.keys(v).map(col=>typeof(this.state.LayoutColunas[col]) !== 'undefined' ? <td>{v[col]}</td>: '')}
                                        </tr>
                                      )
                                    }else{
                                        return(   <tr>
                                                <td><input type='checkbox' name='marcacao' onBlur={()=>this.incluirExclusao(v.id)} value={v.id}/> {k}</td>
                                                {this.state.colunas.map(col=>typeof(this.state.LayoutColunas[col]) !== 'undefined' ? <td><InputVenda col={col} id={v.id} onBlur={this.EnviarMudanca} type='text' value={v[col]}/></td>: '')}
                                        </tr> )
                                        }   
                                    })
                             } 
                            </table>
                            <AddItem onClick={this.adicionarVenda} dados={this.state.vendas} pai={this}/>
                            <ExcItem onClick={this.excluirVenda} dados={this.state.vendas} pai={this}/>
					</Grid>
		)
	}
}
class InputVenda extends React.Component{
	
	constructor(props){
        super(props)
        this.state = {valor:this.props.value}
        this.preencherValor = this.preencherValor.bind(this)
    }

    preencherValor(e){
        this.setState({...this.state, valor:e.target.value})
    }
	render(){
		return(
                    <input type={this.props.type} 
                    onChange={this.preencherValor} 
                    value={this.state.valor} 
                    onBlur={()=>this.props.onBlur(this.props.id, this.props.col, this.state.valor)} 
                    style={{padding:'0',margin:'0',border:'0'}} >
                    </input> 
		)
	}
}
export default Venda


/*
	render(){
        console.log(this.state.listaExclusao)
		return(
					<Grid cols='12'>
                        <h3>Vendas</h3>
                        <table className='table table-sm'>
                            <tr>
                            <td>Item</td>
                            {this.state.colunas.map(col=>typeof(this.state.LayoutColunas[col]) !== 'undefined' ? <td>{this.state.LayoutColunas[col].label}</td>: '')}
                            </tr>
                            
                            {
                                this.state.vendas.map((v,k) => {
                                    this.valorTotal = k==0?
                                    !v.valor ? 0 : v.valor:
                                    this.valorTotal + (!v.valor ? 0 : v.valor)

                                 return(   <tr>
                                        <td><input type='checkbox' name='marcacao' onChange={()=>this.incluirExclusao(v.id)} value={v.id}/> {++k}</td>
                                        {this.state.colunas.map(col=>typeof(this.state.LayoutColunas[col]) !== 'undefined' ? <td><InputVenda onChange={this.EnviarMudanca(col)} type='text' value={v[col]}/></td>: '')}
                                 </tr> )   
                                })
                             }

                                <tr><td colspan='3'>Total</td><td colspan='3'>R$ {this.valorTotal} </td></tr>  
                            </table>
                            <AddItem onClick={this.adicionarVenda} dados={this.state.vendas} pai={this}/>
                            <ExcItem onClick={this.excluirVenda} dados={this.state.vendas} pai={this}/>
					</Grid>
		)
	}
}

*/



