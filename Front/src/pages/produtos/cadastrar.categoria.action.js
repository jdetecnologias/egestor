import axios from 'axios'
import config from '../../config/config'

const BASE_URL = config.base_url_api


export  function manterCategoria(Categoria){
	console.log(Categoria)
	return (dispatch, getState)=>{
		if(Categoria.atualizacao){
			if(Categoria.kind == 'categoria'){
				axios.put(`${BASE_URL}/categoria/${Categoria._id}`,Categoria,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarCategorias()))
			}else{
				axios.put(`${BASE_URL}/subcategoria/${Categoria._id}`,Categoria,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarSubCategorias()))
			}
		}else{
			if(Categoria.kind == 'categoria'){
				axios.post(`${BASE_URL}/categoria`,Categoria,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarCategorias()))
			}else{
				axios.post(`${BASE_URL}/subcategoria`,Categoria,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarSubCategorias()))
			}
		}
	}
}

export  function excluirCategoria(id,kind){
	return (dispatch, getState)=>{
		if(kind === 'categoria'){
			axios.delete(`${BASE_URL}/categoria/${id}`,{id},{headers: {"Content-Type": "application/json;charset=UTF-8"}}).then(resp=>dispatch(importarCategorias()))
		}else{
			axios.delete(`${BASE_URL}/subcategoria/${id}`,{id},{headers: {"Content-Type": "application/json;charset=UTF-8"}}).then(resp=>dispatch(importarSubCategorias()))
		}
	}
} 

export const importarCategorias = () => {
	return (dispatch, getState)=>{		
	const request = axios.get(`${BASE_URL}/categoria`)
	.then(res=>dispatch({type: 'IMPORT_CATEGORY', payload: res.data}))
	}
}

export const importarSubCategorias = () => {
	return (dispatch, getState)=>{	
	const request = axios.get(`${BASE_URL}/subcategoria`)
	.then(res=>dispatch({type: 'IMPORT_SUB_CATEGORY', payload: res.data}))
	}
}