import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {Input,Form, Select} from '../../common/layout/form'
import Grid from '../../common/layout/grid'
import Page from '../../common/layout/page'
import Row from '../../common/layout/row'
import AutoComplete from '../../common/widget/autoComplete'
import SelectMItens from '../../common/layout/SelectMultiItens'
import {gravarProduto,importarProdutos} from './cadastrar.produto.action'
import {limparVariante} from './variacao.produtos.action'
import {importarVariantes} from './cadastrar.variante.action'
import {importarCategorias as impCat, importarSubCategorias as impSub} from './cadastrar.categoria.action'
import {inverterArray,resetarCamposSelect} from '../../common/operator/funcoes'

class CadastroProduto extends React.Component{
	constructor(props){
		super(props)
		this.state = this.limparState()
		this.definirProduto = this.definirProduto.bind(this)
		this.validarForm = this.validarForm.bind(this)
		this.ResetarSelects = this.ResetarSelects.bind(this)
		this.cancelarCadastro = this.cancelarCadastro.bind(this)
		this.defProduto = this.defProduto.bind(this)
		this.getInfoProdutos = this.getInfoProdutos.bind(this)
		this.salvarItemSelecionado = this.salvarItemSelecionado.bind(this)
		this.gravarProduto = this.gravarProduto.bind(this)
	}
	limparState(){
		let obj = {descricao:{value:'',label:''},variantes:[]} 
		return obj

	}
	
	defProduto(subItem){
		let produto = this.state.produto
		
		produto.push(subItem)
		this.setState({...this.state, produto})
	}

	gravarProduto(){
		let produto = {}
		const info = this.getInfoProdutos()
		let variant = []
		const variants = this.state.variantes
		Object.keys(variants).forEach((key,i,arr) => {
			let obj = {}
			obj.nomeVariante = key
			obj.itens = variants[key]
			obj.itens.map((o,i)=>{
				console.log(o)
				obj.itens[i].id = o._id
			})
			variant.push(obj)
			console.log(obj)
		})
		
		produto.variantes = variant
		produto.descricaoCompleta = info.descricaoCompleta
		produto.codigo = info.codigo

	this.props.gravarProduto(produto)
	}

	salvarItemSelecionado(tipo,item, multi = false){
		let variante = this.state.variantes;
		if(multi && variante[tipo]){
			const indice = variante[tipo].findIndex(function(el, index, array){
				return el._id === item._id
			})
			if(indice >= 0){
				variante[tipo].splice(indice,1)
			}
			else{
				variante[tipo].push(item)
			}
		}else{
			variante[tipo] = []
			variante[tipo].push(item)
		}
		this.setState({...this.state, variantes:variante})
		
		
	}
	definirProduto(e){
		const campo = e.target.getAttribute('tipo') || e.target.getAttribute('id') 
		let state = this.state
		state[campo] = {value: '', label:''}
		let elemento = e.target
		state.descricao.value = elemento.value
		state.descricao.label = elemento.value
		this.setState(state)
	}
	
	getInfoProdutos(){
		const state = this.state
		 const codigo = state.variantes.categoria[0].alias 		+
						state.variantes.subcategoria[0].alias 	+
						this.props.cadastro.produtos.length

		const descricaoCompleta = state.variantes.categoria[0].nome		+' '+
								  state.variantes.subcategoria[0].nome	+' '+
								  state.descricao.label 

		return {codigo,descricaoCompleta}

	}
	 

	
	validarForm(){
		const estado = this.state;
		const containerValidate = estado.variantes.categoria && estado.variantes.categoria[0].nome != ''
		const subcontainerValidate = estado.variantes.subcategoria && estado.variantes.subcategoria[0].nome != ''
		const descValidate = estado.descricao.value !== '' && estado.descricao.label !== '' && estado.descricao.value && estado.descricao.label
		
		
		if(containerValidate && subcontainerValidate && descValidate){
			let produtos = this.props.cadastro.produtos;
			this.gravarProduto()
			this.setState(this.limparState())

		}
		else {
			alert('Há campos que ainda não foram preenchidos, favor corrigir e tentar novamente!')
		}
	}
	
	cancelarCadastro(){
		this.props.limparVariante()
		this.setState(this.limparState())
	}
	
	ResetarSelects(){
		const campos = this.state.campos;		
		resetarCamposSelect(campos)
	}
	
	componentDidMount(){
		this.props.importarVariantes()
		this.props.importarProdutos()
		this.props.impCat()
		this.props.impSub()
		this.setState(this.limparState())
	}
	
	render(){
		const qtdProd = this.props.cadastro.produtos.length
		const produtos = this.props.cadastro.produtos || []
		return (
			<Page cols='11' title='Cadastrar produto'>
			<Row>
				<Grid cols='12 10 8 6'>
					<h3 className='text-center'>Cadastrar Produto</h3>
					<Form cols='12'>
								<AutoComplete
									tipo='categoria'
									cols='12 6' 
									id='categoria'
									text='categoria' 
									label='categoria'
									onClickItem={this.salvarItemSelecionado} 
									pArrayList={this.props.cadastro.categorias} 
									field='nome' 
									fieldValue='alias' 
									selectedItens = {this.state.variantes}
								minLength={1}/>

								<AutoComplete
									tipo='subcategoria'
									cols='12 6' 
									id='subcategoria'
									text='subcategoria'
									label='subcategoria'
									onClickItem={this.salvarItemSelecionado} 
									pArrayList={this.props.cadastro.subcategorias} 
									field='nome' 
									fieldValue='alias' 
									selectedItens = {this.state.variantes}
								minLength={1}/>								
					<Input 
						cols='12' 
						placeholder='Descrição produto' 
						label='Produto' 
						id='descricao' 
						onChange={this.definirProduto} 
						valor={this.state.descricao.value}
					/>	
					{

						this.props.cadastro.variantes.map((variante,key)=>{
						return (
								<AutoComplete
									tipo={variante.nomeVariante}
									cols='12 6' 
									id={variante.nomeVariante}
									text={variante.nomeVariante} 
									label={variante.nomeVariante} 
									onClickItem={this.salvarItemSelecionado} 
									pArrayList={variante.itens} 
									field='valor' 
									fieldValue='alias' 
									multi={true}
									selectedItens = {this.state.variantes}
								minLength={1}/>	)
					})
					}
					<Grid cols='12'>
						<h4 className='text-center'>Código Produto</h4>
						<p>
							Código : 	{this.state.variantes.categoria? this.state.variantes.categoria[0].alias: ''  }
									{this.state.variantes.subcategoria? this.state.variantes.subcategoria[0].alias: ''}
									{this.props.cadastro.produtos.length }		
						</p>
						<p>
							Descrição:{this.state.variantes.categoria? this.state.variantes.categoria[0].nome+' ': ''  }
									{this.state.variantes.subcategoria? this.state.variantes.subcategoria[0].nome+ ' ': ''}
									{this.state.descricao.label }
						</p>
					</Grid>
				</Form>
				<button className='btn btn-primary btn-sm mx-1' onClick={this.validarForm}>Cadastrar</button>
				<button className='btn btn-danger btn-sm mx-1' onClick={this.cancelarCadastro}>Cancelar</button>				
				</Grid>
				<Grid cols='12 10 8 6'>
					<h2 className='text-center'>Produtos</h2>
					<table  className='table'>
						<thead>
							<tr>
								<th>Código</th>
								<th>Descrição</th>
							</tr>
						</thead>
						<tbody>
						{
							produtos.map((produto,key)=>(
								<tr key={key} indice={key}>
									<td>{produto.codigo}</td>
									<td>{produto.descricaoCompleta}</td>
								</tr>
							)
							
)
						}
						</tbody>
						
					</table>
				</Grid>
			</Row>
			</Page>
		
		)
	}
}

const mapStateToProps = state => ({cadastro: state.cadastroProduto, variacao: state.variacao})
const  mapDispatchToProps = dispatch => bindActionCreators({gravarProduto,importarProdutos,importarVariantes,impCat, impSub,limparVariante},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(CadastroProduto)

/*
					<AutoComplete
					tipo='categoria'
					cols='12 6' 
					text={this.state.container.label} 
					label='Categoria' 
					onClickItem={this.definirProduto} 
					pArrayList={this.props.cadastro.containers} 
					field='label' 
					fieldValue='value' 
					minLength={1}/>
					
					<AutoComplete
					tipo='subcategoria'
					cols='12 6' 
					text={this.state.subcontainer.label} 
					label='Subcategoria' 
					onClickItem={this.definirProduto} 
					pArrayList={this.props.cadastro.subcontainers} 
					field='label' 
					fieldValue='value' 
					minLength={1}/>
					
										<AutoComplete
					tipo='cor'
					cols='12 6' 
					text={this.state.cor.label} 
					label='Cor' 
					onClickItem={this.definirProduto} 
					pArrayList={this.props.cadastro.cores} 
					field='label' 
					fieldValue='value' 
					minLength={1}/>					


*/