import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {Input,Form, Select} from '../../common/layout/form'
import Grid from '../../common/layout/grid'
import Page from '../../common/layout/page'
import Row from '../../common/layout/row'
import If from '../../common/operator/if'
import AutoComplete from '../../common/widget/autoComplete'
import {gravarVariante, importarVariantes,atualizarVariante, excluirVariante} from './cadastrar.variante.action'
import {inverterArray,resetarCamposSelect,getAlias} from '../../common/operator/funcoes'

class CadastroVariante extends React.Component{
	constructor(props){
		super(props)
		this.state = this.limparState()
		this.definirTexto = this.definirTexto.bind(this)
		this.enviarVariante = this.enviarVariante.bind(this)
		this.adicionarItem = this.adicionarItem.bind(this)
		this.validarItem = this.validarItem.bind(this)
		this.validarNomeVariante = this.validarNomeVariante.bind(this)
		this.setAliasToItemVariante = this.setAliasToItemVariante.bind(this)
		this.editarItemVariante = this.editarItemVariante.bind(this)
		this.cancelarInclusaoItem = this.cancelarInclusaoItem.bind(this)
		this.excluirItemVariante = this.excluirItemVariante.bind(this)
		this.editarVariante = this.editarVariante.bind(this)
		this.excluirVariante = this.excluirVariante.bind(this)
		this.resetState = this.resetState.bind(this)
	}
	
	excluirVariante(){
		const response  = window.confirm('Deseja realmente apagar a variante ?')
		if(response){
			this.props.excluirVariante(this.state._id)
		}
	}
		
	excluirItemVariante(){
		const response = window.confirm('Você tem certeza que deseja excluir esse item?')
		if(response){
			const state = this.state
			let itens = state.itens
			const indice = state.indice
			itens.splice(indice,1)
			state.itemVariante = {valor:'', alias:''}
			state.atualizacao = false
			state.indice = null
			state.itens = itens
			this.setState({...this.state, state})
		}
	}
	editarItemVariante(indice){
		const itens = [].concat(this.state.itens)
		const item = JSON.parse(JSON.stringify(itens[indice]))
		this.setState({...this.state, atualizacao:true,itemVariante: item,indice})	
	}
	cancelarInclusaoItem(){
				const itemVariante = {valor:'', alias:''}
				const atualizacao = false
				const indice = null
				this.setState({...this.state, itemVariante, atualizacao, indice})		
	}
	adicionarItem(){
		if(this.validarItem()){
			if(this.state.atualizacao){
				let indice = this.state.indice
				let itens = this.state.itens	
				itens[indice] = this.state.itemVariante
				itens[indice].alias =  getAlias(this.state.itemVariante.valor)		
				const itemVariante = {valor:'', alias:''}
				const atualizacao = false
				indice = null
				this.setState({...this.state, itemVariante,itens,atualizacao, indice})
			}
			else{
				let state = this.state
				state.itemVariante.alias = getAlias(this.state.itemVariante.valor)
				state.itens.push(state.itemVariante)
				state.itemVariante = {valor:'', alias:''}
				this.setState({...this.state, state})
			}
		}
	}
	editarVariante(indice){
		const variante = this.props.cadastro.variantes[indice]
		variante.itemVariante = {valor:'', alias:''}
		variante.indiceVariante = indice
		variante.atualizarVariante = true

		
		this.setState(variante)
		
	}
	limparState(){
		return  {nomeVariante:'',itens:[], itemVariante:{valor:'', alias:''},atualizacao:false,indice:null,atualizarVariante: false, indiceVariante:null}
	}
	resetState(){
		this.setState(this.limparState())
	}
	definirTexto(e){
		const campo = e.target.getAttribute('tipo') || e.target.getAttribute('id') 
		let state = this.state
		let elemento = e.target
		switch(campo){
			case "variante":
			 state.nomeVariante = elemento.value
			break
			case "valor":
			 state.itemVariante.valor = elemento.value
			break		
		}
		this.setState({...this.state,state})
	}
	setAliasToItemVariante(){
		const state = this.state
		const alias = state.itemVariante.valor.length > 3 ?state.itemVariante.valor.trim().replace(" ","").slice(0,3):state.itemVariante.valor
		return alias
	}
	 validarItem(){
		 if(this.state.itemVariante.valor === '' ||  !this.validarNomeVariante()){
			 window.alert('Favor preencher corretamente o fomulário')
			 return false
		 }
		 else{
			 return true
		 }
	 }
	validarNomeVariante(){
		if(this.state.nomeVariante === ''){
			return false
		}
		else {
			return true		
			}
	}
	enviarVariante(){		
		if(this.validarNomeVariante()){
			let state = this.state
			delete state.itemVariante
			console.log(state)
			this.props.gravarVariante(state)
			this.setState(this.limparState())

		}
		else {
			alert('Há campos que ainda não foram preenchidos, favor corrigir e tentar novamente!')
		}
	}
	componentDidMount(){
		this.props.importarVariantes()
	}
	render(){
		return (
			<Page cols='12' title='Cadastrar Variante de Produto'>
				<Row>
					<Grid cols='12 10 8 6'>
						<h3 className='text-center'>Cadastrar Variante</h3>
						
						<Input 
							cols='12' 
							placeholder='Nome da variante' 
							label='Variante' 
							id='variante' 
							onChange={this.definirTexto} 
							valor={this.state.nomeVariante}
						/>			
						<Row>
							<Grid cols='12 6'>
								<Input 
									cols='12' 
									placeholder='Valor' 
									label='Valor' 
									id='valor' 
									onChange={this.definirTexto} 
									valor={this.state.itemVariante.valor}
								/>	

									<button className='btn btn-primary my-2 btn-sm mx-1' onClick={this.adicionarItem}>{this.state.atualizacao?'Atualizar':'Adicionar'}</button>
									<button className='btn btn-danger my-2 btn-sm mx-1' onClick={this.cancelarInclusaoItem}>Cancelar</button>
									<If test={this.state.atualizacao}>
										<button className='btn btn-danger btn-sm mx-1' onClick={this.excluirItemVariante}>Excluir</button>
									</If>

							
							</Grid>
							<Grid cols='12 6'>
								<table className='table table-sm table-bordered' style={{position:'absolute', zIndex:'9999', backgroundColor:'white'}}>
									<tr className='bg-primary text-white'><th className='text-center' colspan='2'>Itens da variante</th></tr>
									{
										this.state.itens.map((item, key)=>{
											return <tr onClick={()=>this.editarItemVariante(key)} style={{cursor:'pointer'}}>
													<td className='text-center'>{item.alias}</td>
													<td className='text-center'>{item.valor}</td>
												</tr>
										})
									}
								</table>
							</Grid>
						</Row>
					<If test={!this.state.atualizarVariante}>						
						<button className='btn btn-primary btn-sm mx-1' onClick={this.enviarVariante}>Cadastrar</button>					
					</If>
					<If test={this.state.atualizarVariante}>
						<button className='btn btn-primary btn-sm mx-1' onClick={this.enviarVariante}>Atualizar</button>
						<button className='btn btn-danger btn-sm mx-1' onClick={this.excluirVariante}>Excluir</button>
					</If>
					<button className='btn btn-danger btn-sm mx-1' onClick={this.resetState}>Cancelar</button>						
					</Grid>
					<Grid cols='12 10 8 6'>
						<h2 className='text-center'>Variantes do produto</h2>
						<table  className='table'>
							<thead>
								<tr>
									<th>Variantes</th>
								</tr>
							</thead>
							<tbody>
							{
								this.props.cadastro.variantes.map((variante,key)=>(
									<tr style={{cursor:'pointer'}} key={key} onClick={()=>this.editarVariante(key)} indice={key}>
										<td>{variante.nomeVariante}</td>
									</tr>
								)
								
	)
							}
							</tbody>
							
						</table>
					</Grid>
				</Row>
			</Page>
		
		)
	}
}

const mapStateToProps = state => ({cadastro: state.cadastroProduto})
const  mapDispatchToProps = dispatch => bindActionCreators({gravarVariante,importarVariantes,atualizarVariante, excluirVariante},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(CadastroVariante)

/*					<Select 
						cols='12 6' 
						options={this.props.cadastro.containers} 
						id='categoria' label='Categoria' 
						onChange={this.definirProduto}
					/>
					<Select cols='12 6' 
						options={this.props.cadastro.subcontainers} 
						id='subcategoria' 
						label='SubCategoria' 
						onChange={this.definirProduto}
					/>
					<Select cols='12 6' options={this.props.cadastro.cores} label='Cor' id='cor'onChange={this.definirProduto}/>	
				<Grid cols='12 10 8 6'>
					<h2 className='text-center'>Produtos</h2>
					<table  className='table'>
						<thead>
							<tr>
								<th>Código</th>
								<th>Descrição</th>
							</tr>
						</thead>
						<tbody>
						{
							produtos.map((produto,key)=>(
								<tr key={key} indice={key}>
									<td>{produto.codigo}</td>
									<td>{produto.descricaoCompleta}</td>
								</tr>
							)
							
)
						}
						</tbody>
						
					</table>
				</Grid>
					
					*/