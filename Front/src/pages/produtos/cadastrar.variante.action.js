import axios from 'axios'
import config from '../../config/config'

const BASE_URL = config.base_url_api


export  function gravarVariante(Variante){
	return (dispatch, getState)=>{
		if(Variante._id){
			axios.put(`${BASE_URL}/variante/${Variante._id}`,Variante,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarVariantes()))
		}else{
			axios.post(`${BASE_URL}/variante`,Variante,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarVariantes()))			
		}
		
	}
}

export  function excluirVariante(id){
	return (dispatch, getState)=>{
	axios.delete(`${BASE_URL}/variante/${id}`,{id},{headers: {"Content-Type": "application/json;charset=UTF-8"}}).then(resp=>dispatch(importarVariantes()))
	}
} 

export  function atualizarVariante(id){
	return (dispatch, getState)=>{
	axios.put(`${BASE_URL}/variante/${id}`,{headers:{'Content-Type': 'application/json'}}).then(resp=>dispatch(importarVariantes()))
	}
}

export const importarVariantes = () => {
	return (dispatch, getState)=>{
		
	const request = axios.get(`${BASE_URL}/variante`)
	.then(res=>dispatch({type: 'IMPORT_VARIANTES', payload: res.data}))

	}
}