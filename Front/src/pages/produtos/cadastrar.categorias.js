import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {Input,Form, Select} from '../../common/layout/form'
import Grid from '../../common/layout/grid'
import Page from '../../common/layout/page'
import Row from '../../common/layout/row'
import If from '../../common/operator/if'
import AutoComplete from '../../common/widget/autoComplete'
import {importarCategorias,importarSubCategorias, manterCategoria, excluirCategoria} from './cadastrar.categoria.action'
import {inverterArray,resetarCamposSelect,getAlias} from '../../common/operator/funcoes'

class CadastroVariante extends React.Component{
	constructor(props){
		super(props)
		this.state = this.limparState()
		this.definirTexto = this.definirTexto.bind(this)
		this.adicionarItem = this.adicionarItem.bind(this)
		this.validarItem = this.validarItem.bind(this)
		this.setAliasToItemVariante = this.setAliasToItemVariante.bind(this)
		this.excluirCategoria = this.excluirCategoria.bind(this)
		this.editarCategoria = this.editarCategoria.bind(this)
		this.resetState = this.resetState.bind(this)
	}
	
	excluirCategoria(){
		const response  = window.confirm(`Deseja realmente apagar a ${this.state.kind} ?`)
		if(response){
			this.props.excluirCategoria(this.state._id,this.state.kind)
			this.resetState()
		}
	}
		
	adicionarItem(){
		const state = this.state 
		let Categoria
		const add = {nome: state.nome, alias: state.alias, kind:state.kind, atualizacao: state.atualizacao}
		state.atualizacao? Categoria =  state : Categoria = add
		this.props.manterCategoria(Categoria)
		this.resetState()
	}
	
	editarCategoria(campo,indice){
		let categoria
		if(campo == "categoria"){
			categoria = this.props.cadastro.categorias[indice]
		}else if(campo == "subcategoria"){
			categoria = this.props.cadastro.subcategorias[indice]
		}
		categoria.indice = indice
		categoria.kind = campo
		categoria.atualizacao = true
		this.setState(categoria)
	}
	
	limparState(){
		return  {nome:'',alias: '', kind:'categoria',atualizacao:false}
	}
	
	resetState(){
		this.setState(this.limparState())
	}
	
	definirTexto(e){
		const campo = e.target.getAttribute('tipo') || e.target.getAttribute('id') 
		let state = this.state
		let elemento = e.target
		switch(campo){
			case "nome":
			 state.nome = elemento.value
			 state.alias = getAlias(elemento.value)
			break		
		}
		this.setState({...this.state,state})
	}
	
	setAliasToItemVariante(){
		const state = this.state
		const alias = state.nome.length > 3 ?state.nome.trim().replace(" ","").slice(0,3):state.nome
		return alias.toLowerCase()
	}
	
	 validarItem(){
		 if(this.state.nome === '' ){
			 window.alert('Favor preencher corretamente o fomulário')
			 return false
		 }
		 else{
			 return true
		 }
	 }

	componentDidMount(){
		this.props.importarCategorias()
		this.props.importarSubCategorias()		
	}
	
	render(){
		return (
			<Page cols='12' title='Categorias de produtos'>
				<Row>
					<Grid cols='12 10 8 6'>
						<h3 className='text-center'>Manter {this.state.kind}</h3>	
						<input type='radio' name='kind' checked={this.state.kind === 'categoria'} className='mx-1' onChange={(e)=>this.setState({...this.state, kind: e.target.value})} value='categoria'/> Categoria 
						<input type='radio' name='kind'  checked={this.state.kind === 'subcategoria'} className='mx-1' onChange={(e)=>this.setState({...this.state, kind: e.target.value})} value='subcategoria'/> Subcategoria
						<Row>
							<Grid cols='12 6'>
								<Input 
									cols='12' 
									placeholder={`nome da ${this.state.kind}`} 
									label='Nome' 
									id='nome' 
									onChange={this.definirTexto} 
									valor={this.state.nome}
								/>	
									<button className='btn btn-primary my-2 btn-sm mx-1' onClick={this.adicionarItem}>{this.state.atualizacao?'Atualizar':'Adicionar'}</button>
									<button className='btn btn-danger my-2 btn-sm mx-1' onClick={this.resetState}>Cancelar</button>
									<If test={this.state.atualizacao}>
										<button className='btn btn-danger btn-sm mx-1' onClick={this.excluirCategoria}>Excluir</button>
									</If>
							</Grid>
						</Row>					
					</Grid>
					<Grid cols='3'>
						<table  className='table'>
							<thead>
								<tr>
									<th>Alias</th>
									<th>Categorias</th>
								</tr>
							</thead>
							<tbody>
							{
								this.props.cadastro.categorias.map((categoria,key)=>(
									<tr style={{cursor:'pointer'}} key={key} onClick={()=>this.editarCategoria("categoria",key)} indice={key}>
										<td>{categoria.alias}</td>
										<td>{categoria.nome}</td>
									</tr>
								)
								
	)
							}
							</tbody>
							
						</table>
					</Grid>
					<Grid cols='3'>
						<table  className='table'>
							<thead>
								<tr>
									<th>Alias</th>
									<th>Subcategorias</th>
								</tr>
							</thead>
							<tbody>
							{
								this.props.cadastro.subcategorias.map((subcategoria,key)=>(
									<tr style={{cursor:'pointer'}} key={key} onClick={()=>this.editarCategoria("subcategoria",key)} indice={key}>
										<td>{subcategoria.alias}</td>
										<td>{subcategoria.nome}</td>
									</tr>
								)
								
	)
							}
							</tbody>					
						</table>
					</Grid>									
				</Row>
			</Page>
		)
	}
}

const mapStateToProps = state => ({cadastro: state.cadastroProduto})
const  mapDispatchToProps = dispatch => bindActionCreators({ importarCategorias,importarSubCategorias,manterCategoria, excluirCategoria},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(CadastroVariante)

/*					<Select 
						cols='12 6' 
						options={this.props.cadastro.containers} 
						id='categoria' label='Categoria' 
						onChange={this.definirProduto}
					/>
					<Select cols='12 6' 
						options={this.props.cadastro.subcontainers} 
						id='subcategoria' 
						label='SubCategoria' 
						onChange={this.definirProduto}
					/>
					<Select cols='12 6' options={this.props.cadastro.cores} label='Cor' id='cor'onChange={this.definirProduto}/>	
				<Grid cols='12 10 8 6'>
					<h2 className='text-center'>Produtos</h2>
					<table  className='table'>
						<thead>
							<tr>
								<th>Código</th>
								<th>Descrição</th>
							</tr>
						</thead>
						<tbody>
						{
							produtos.map((produto,key)=>(
								<tr key={key} indice={key}>
									<td>{produto.codigo}</td>
									<td>{produto.descricaoCompleta}</td>
								</tr>
							)
							
)
						}
						</tbody>
						
					</table>
				</Grid>
					
					*/