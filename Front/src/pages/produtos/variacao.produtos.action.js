export function definirVariacao(payload){
    return {type: 'SET_VARIANT', payload}
}

export function limparVariante(){
    return {type:'RESET_VARIANT',payload:{}}
}