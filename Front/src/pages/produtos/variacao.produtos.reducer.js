const INITIAL_STATE = {variantes:{}}

export default function(state = INITIAL_STATE, action){
    switch(action.type){
        case 'SET_VARIANT':
            return {...state, variantes: action.payload}          
        case 'RESET_VARIANT':
            return {...state, variantes: action.payload} 

        default:
            return state
    }
}