import React from 'react'
import Grid from '../common/layout/grid'
import Page from '../common/layout/page' 
import Row from '../common/layout/row'
import If from '../common/operator/if'
import {Input, Form,Select} from '../common/layout/form'
import Vendas from './vendas'
import Financeiro from './financeiro'
import Compras from './compras'
import Clientes from './clientes'
import Fornecedores from './fornecedores'
import DadosCadastrais from './dados_cadastrais'
import {optionProdutos,resetarCamposSelect,obterTamanhoGrid as OBT,controlarTamanhoTela as CTT} from '../common/operator/funcoes'

class Painel extends React.Component{
	
	constructor(props){
        super(props)
        this.state = {abaAtual:1}
        /* Telas
        || 1-Vendas 
        || 2-Financeiro 
        || 3-Compras 
        || 4-Clientes 
        || 5-Fornecedores 
        || 6-Dados Cadastrais
        */
       this.mudarAba = this.mudarAba.bind(this)
    }

    mudarAba(numAba){
        this.setState({abaAtual: numAba})
    }

	render(){
		return(
			<Page cols='12' title='Painel de Gestão rápido'>
				<Row>
					<Grid cols='12 10 8 6'>                     
                        <If test={this.state.abaAtual == 1}>
                          <Vendas/>
                        </If>
                        <If test={this.state.abaAtual == 2}>
                            <Financeiro/>
                        </If>
                        <If test={this.state.abaAtual == 3}>
                            <Compras/>
                        </If>
                        <If test={this.state.abaAtual == 4}>
                            <Clientes/>
                        </If>
                        <If test={this.state.abaAtual == 5}>
                            <Fornecedores/>
                        </If>                       
                        <If test={this.state.abaAtual == 6}>
                            <DadosCadastrais/>
                        </If>                                                                                                                       
					</Grid>                    
				</Row>
                <div className='my-2'>
                    <button onClick={()=>this.mudarAba(1)} className={`btn btn-${this.state.abaAtual == 1 ?'md':'sm'} btn-primary`}>Vendas</button>
                    <button onClick={()=>this.mudarAba(2)} className={`btn btn-${this.state.abaAtual == 2 ?'md':'sm'} btn-primary`}>Financeiro</button>
                    <button onClick={()=>this.mudarAba(3)} className={`btn btn-${this.state.abaAtual == 3 ?'md':'sm'} btn-primary`}>Compras</button>
                    <button onClick={()=>this.mudarAba(4)} className={`btn btn-${this.state.abaAtual == 4 ?'md':'sm'} btn-primary`}>Clientes</button>
                    <button onClick={()=>this.mudarAba(5)} className={`btn btn-${this.state.abaAtual == 5 ?'md':'sm'} btn-primary`}>Fornecedores</button>
                    <button onClick={()=>this.mudarAba(6)} className={`btn btn-${this.state.abaAtual == 6 ?'md':'sm'} btn-primary`}>Dados Cadastrais</button>                 
                </div>
            </Page>
		)
	}
}

export default Painel