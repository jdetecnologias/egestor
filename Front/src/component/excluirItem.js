import React from 'react'
import Grid from '../common/layout/grid'
import Page from '../common/layout/page' 
import Row from '../common/layout/row'
import If from '../common/operator/if'
import {Input, Form,Select} from '../common/layout/form'
import {optionProdutos,resetarCamposSelect,obterTamanhoGrid as OBT,controlarTamanhoTela as CTT} from '../common/operator/funcoes'

class AddItem extends React.Component{
	
	constructor(props){
		super(props)
		this.excluirItem = this.excluirItem.bind(this)
    }
	
	excluirItem(){
	    this.props.onClick();
	}
	render(){
		return(
                <button onClick={this.excluirItem} className='btn btn-danger mx-1 btn-sm'>Excluir</button>
		)
	}
}

export default AddItem

