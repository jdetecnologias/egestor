import React from 'react'
import Grid from '../common/layout/grid'
import Page from '../common/layout/page' 
import Row from '../common/layout/row'
import If from '../common/operator/if'
import {Input, Form,Select} from '../common/layout/form'
import {optionProdutos,resetarCamposSelect,obterTamanhoGrid as OBT,controlarTamanhoTela as CTT} from '../common/operator/funcoes'

class AddItem extends React.Component{
	
	constructor(props){
		super(props)
		this.adicionarItem = this.adicionarItem.bind(this)
    }
	
	adicionarItem(){
		const pai = this.props.pai
	/*	let pArray = this.props.dados
		let obj = {};
		
		Object.keys(pArray).forEach(chave => {
			obj[chave] = ''
		});
		pArray.push(obj)
	*/
	this.props.onClick();
	
	//	pai.setState({...pai.state})
	}
	render(){
		return(
                <button onClick={this.adicionarItem} className='btn btn-primary btn-sm'>Adicionar</button>
		)
	}
}

export default AddItem

