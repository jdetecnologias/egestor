module.exports = function () {
var sqlite = require('sqlite-sync')

sqlite.connect('./bd/egestor.db')

     criarTabela = function (){
      sqlite.run('create table if not exists vendas(id integer primary key autoincrement not null, cliente varchar(255), valor float, data_venda varchar(10), data_pagamento varchar(255), status varchar(255))')
    }

     gravarVenda = function (){
        sqlite.run("insert into vendas(cliente, valor, data_venda, data_pagamento, status) values (?,?,?,?,?)",['',0,'','',''],function(id){
        })
    }

     obterTodasVendas = function(){
        const vendas = sqlite.run("select * from vendas where status <> '0'")
        return vendas
    }

    apagarVendas = function(ids){
        sqlite.run("update vendas set status = 0 where id in("+ids+")")
    }

    atualizarColuna = function(id, coluna, valor){

        const query =   "update vendas set "+coluna+" = '"+valor+"' where id in("+id+")"
        sqlite.run(query)  
    }

    return {criarTabela, gravarVenda, obterTodasVendas,apagarVendas, atualizarColuna}
}

