const restify = require('restify')
const porta = 3003
const  servidor = restify.createServer()
const cors = require('./cors') 
const vendas = require('./model/vendas.model.js')

servidor.pre(cors.preflight)
servidor.use(cors.actual)

servidor.get('/', (req,res)=>{
    res.send('Tete')
})

servidor.get('/vendas', (req,res)=>{
    const sell = vendas();
    res.json(sell.obterTodasVendas())
})

servidor.post('/vendas', (req,res)=>{
    const sell = vendas();

    sell.gravarVenda()
    res.json(sell.obterTodasVendas())

 })

 servidor.put('/vendas/:id/:col/:valor', (req,res)=>{
    const sell = vendas()
    sell.atualizarColuna(req.params.id, req.params.col, req.params.valor)
    res.json(sell.obterTodasVendas())

 })
 servidor.del('/vendas/:ids', (req,res)=>{
    const sell = vendas();
    sell.apagarVendas(req.params.ids)
    res.json(sell.obterTodasVendas())

 })

servidor.get('/financeiro', (req,res)=>{
    res.send('financeiro')
})

servidor.get('/compras', (req,res)=>{
    res.send('compras')
})

servidor.get('/dadoscadastrais', (req,res)=>{
    res.send('dadoscadastrais')
})
servidor.get('/clientes', (req,res)=>{
    res.send('clientes')
})
servidor.get('/fornecedores', (req,res)=>{
    res.send('fornecedores')
})





servidor.listen(porta,()=>{
    vendas().criarTabela();
    console.log('servidor rodando porta '+porta)
})
